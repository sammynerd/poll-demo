// const conn = require("./conn");
const express = require("express");
const ejs = require('ejs');
const fs = require('fs');

const app = express();
createJsonFile();

app.get("/", (req, res) => {

    fs.readFile('poll.json', (err, data) => {
        if (err) throw err;
        let student = JSON.parse(data);
        res.send(student);
    });
});


function createJsonFile() {
  let poll = {
    question: 'How are you?',
    answer: [
        {
            ans1: "Good",
            vote: 0

        },
        {
            ans1: "Bad",
            vote: 0

        }
    ]
  };

  let data = JSON.stringify(poll);
  fs.writeFileSync('poll.json', data);

}

function readJsonFile(){
    fs.readFile('poll.json', (err, data) => {
        if (err) throw err;
        let student = JSON.parse(data);
    });
}

app.listen(3000, function () {
    console.log("Server listening on port 3000");
});
